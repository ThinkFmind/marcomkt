<?php

require '../vendor/autoload.php';

$cfg = require '../src/config.php';

$routes = require '../src/routes.php';

$app = new \Slim\App(['settings' => $cfg]);
$container = $app->getContainer();


//boot eloquent connection
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    //pass the connection to global container (created in previous article)
    $container['db'] = function ($container) use ($capsule){
        return $capsule;
    };


$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../src/templates/');

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    $assetManager = new LoveCoding\TwigAsset\TwigAssetManagement([
        'verion' => '1'
    ]);
    $assetManager->addPath('css', '/css');
    $assetManager->addPath('img', '/images');
    $assetManager->addPath('js', '/js');
    $view->addExtension($assetManager->getAssetExtension());
    return $view;
};

$container['mailer'] = function($container) {
    $twig = $container['view'];
    $mailer = new \Anddye\Mailer\Mailer($twig, [
        'host'      => 'mail.focusmind.net',  // SMTP Host
        'port'      => '465',  // SMTP Port
        'username'  => 'think@focusmind.net',  // SMTP Username
        'password'  => 'ifocusmind123',  // SMTP Password
        'protocol'  => 'SSL'   // SSL or TLS
    ]);

    // Set the details of the default sender
    $mailer->setDefaultFrom('think@focusmind.net', 'Programador');

    return $mailer;
};

$container['EntrevistasController'] = function ($container) {
    return new \App\Controllers\EntrevistasController($container);
};

$container['WebController'] = function ($container) {
    return new \App\Controllers\WebController($container);
};

$container['EntradasController'] = function ($container) {
    return new \App\Controllers\EntradasController($container);
};
$container['UserController'] = function ($container) {
    return new \App\Controllers\UserController($container);
};


$routes($app);

$app->run();



