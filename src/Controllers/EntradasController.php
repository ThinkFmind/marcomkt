<?php


namespace App\Controllers;
use App\Models\Entrada;


class EntradasController extends Controller
{
    public function index($request, $response){

        echo $request->getAttribute('user');
        $data['entradas'] = Entrada::orderBy('id','desc')->get();

        return $this->container->view->render($response, 'back/entrevistas-back.twig',$data);



    }

    public function editEntrada($request, $response,$arg){
        if(!empty($arg)){

            $data['entrada'] = Entrada::find($arg['id']);

            return $this->container->view->render($response, 'back/entrada-edit.twig',$data);

        }

    }

    public function saveEntradaEdit($request, $response){

        $datos = $request->getParsedBody();

        $uploadedFiles = $request->getUploadedFiles();



        $entry = Entrada::find($datos['id']);
        $entry->titulo = $datos['titulo'];
        $entry->descripcion = $datos['descripcion'];
        $entry->year = $datos['year'];
        $entry->editado = 1;

        $entry->save();

        //echo json_encode($entry->toArray());
        $this->index($request, $response);
    }

    public function toggleVisibility($request, $response){
        $datos = $request->getParsedBody();
       /* print_r($datos);
        return false;*/
        if(!empty($datos)){
            $entry = Entrada::find($datos['id']);
           // echo "<pre>";print($entry); echo "</pre>";


            $entry->aprobado = ($datos['aprobado']!='false')?1:0;
            $entry->fecha_aprob = date('Y-m-d');
            $entry->save();
            //echo filter_var($entry->email, FILTER_VALIDATE_EMAIL);
            if(filter_var($entry->email, FILTER_VALIDATE_EMAIL) && $entry->aprobado == 1) {
                //echo 'ENTRO';
                $user = new \stdClass;
                $user->name = $entry->nombre;
                $user->email = $entry->email;
                $user->link = "https://marcomkt.focusmind.net/" . $this->container->router->pathFor('contamos');

                $this->container['mailer']->sendMessage('mails/ok-mail.twig', ['user' => $user], function ($message) use ($user) {
                    $message->setTo($user->email, $user->name);
                    $message->setSubject('Historia aprobada');
                });

                //echo "SEND";
            }
            echo json_encode($entry->toArray());


        }
    }
}