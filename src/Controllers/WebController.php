<?php


namespace App\Controllers;
use App\Models\Entrevista;
use App\Models\Entrada;
use mysql_xdevapi\Exception;
use Slim\Http\UploadedFile;


class WebController extends Controller
{
    public function indexEntrevistas($request, $response, $arg){

        //$type = ($arg['type']=='video')?'V':'P';

        $data['interviews'] = Entrevista::where('visible',1)->get();


        //if($type == 'V'){
            return $this->container->view->render($response, 'entrevistas.twig',$data);
    }

    public function indexNoContamos($request, $response){

        $entradas = Entrada::where('aprobado',1)->orderBy('year','asc')->get();

        $entradas = $entradas->groupBy('year');

        $data['years'] = array_keys($entradas->toArray());

        $data['entradas'] = $entradas;
        return $this->container->view->render($response, 'no-contamos.twig',$data);
    }

    public function showNote($request, $response, $arg){
        if(!$arg['id']){
            echo "error";
            return false;
        }
        $data['nota'] = Entrada::find($arg['id']);

        $prev = Entrada::select('id')->where('aprobado',1)->where('tipo','not')->where('id','<',$arg['id'])->where('year',$data['nota']->year)->latest()->first();
        $next = Entrada::select('id')->where('aprobado',1)->where('tipo','not')->where('id','>',$arg['id'])->where('year',$data['nota']->year)->oldest()->first();

        $data['prev'] = $prev;
        $data['next'] = $next;


        return $this->container->view->render($response, 'notas.twig',$data);
    }

    public function createEntrada($request, $response, $arg){

        $datos = $request->getParsedBody();

        $uploadedFiles = $request->getUploadedFiles();
        $entry = new Entrada();
        $entry->nombre = $datos['nombre'];
        $entry->apellido = $datos['apellido'];
        $entry->email = $datos['email'];
        $entry->titulo = $datos['titulo'];
        $entry->descripcion = $datos['descripcion'];
        $entry->relacion = $datos['relacion'];
        $entry->year = $datos['year'];
        $entry->tipo = $datos['tipo'];
        $entry->fecha_pub = date('Y-m-d');


        $uploadedFiles = $request->getUploadedFiles();
        if(count($uploadedFiles)>0 && $datos['tipo']!='not'){


            $uploadedFile = $uploadedFiles['file'];
           // echo "<pre>";print_r($uploadedFile);echo "</pre>";
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $filename = $this->moveUploadedFile( $uploadedFile);
                $entry->archivo = $filename;
            }
        }



        $entry->save();

        if(filter_var($entry->email, FILTER_VALIDATE_EMAIL)){
            $user = new \stdClass;
            $user->name = $entry->nombre;
            $user->email = $entry->email;
            //$user->link = "https://marcomkt.focusmind.net/".$this->container->router->pathFor('contamos');

            $this->container['mailer']->sendMessage('mails/recept-mail.twig', ['user' => $user], function($message) use($user) {
                $message->setTo($user->email, $user->name);
                $message->setSubject('Historia Recibida');
            });
        }


        //$response->getBody()->write('Mail sent!');

        //return $this->container->view->render($response, 'entrevistas.twig',$data);
    }



    private function moveUploadedFile( UploadedFile $uploadedFile)
    {

        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $folders = array(
            'audio'=>array('mp3','aac','ogg','wav','vorbis'),
            'image'=>array('jpg','jpeg','bmp','png','gif','webp'),
            'video'=>array('mp4','3gp','wmv','mov','MPEG-4','flv','avi','mkv','webm')
        );

        $destFolder = "";

        foreach ($folders as $k=>$v){
            if(in_array($extension,$v)){
                $destFolder = $k;
                break;
            }
        }
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);
        $fileFullName  = "img/uploads/" .$destFolder. DIRECTORY_SEPARATOR . $filename;
        echo $fileFullName;
        $uploadedFile->moveTo($fileFullName);

        return $fileFullName;
    }


}