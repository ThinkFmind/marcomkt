<?php


namespace App\Controllers;
use App\Models\Entrevista;
use Slim\Http\UploadedFile;


class EntrevistasController extends Controller
{
    public function index($request, $response, $arg){
        if(empty($arg['type'])){
            return "ERROR";
        }
        $type = ($arg['type']=='video')?'V':'P';

        $data['data'] = Entrevista::where('tipo',$type)->orderBy('id','desc')->get();

        if($type == 'V'){
            return $this->container->view->render($response, 'back/entrevistas-back-video.twig',$data);
        }else{
            return $this->container->view->render($response, 'back/entrevistas-back-podcast.twig',$data);
        }


    }

    public function createEntrevista($request, $response,$arg){
        $datos = $request->getParsedBody();
        if(!empty($datos)){
            if($datos['id']==""){
                $entrevista = new Entrevista();
            }else{
                $entrevista = Entrevista::find($datos[id]);
            }

            $entrevista->titulo = $datos['titulo'];
            $entrevista->entrevistador = $datos['entrevistado'];
            $entrevista->url_1 = $datos['url-1'];

            $entrevista->descripcion = $datos['descrip'];
            $entrevista->fecha_pub = date('Y-m-d');
            $entrevista->tipo = 'V';
            if(!empty($datos['url-2'])){
                $entrevista->url_2 = $datos['url-2'];
                $entrevista->url_3 = $datos['url-3'];
                $entrevista->tipo = 'P  ';
            }

            $uploadedFiles = $request->getUploadedFiles();
            if(count($uploadedFiles)>0 && $datos['tipo']!='not'){


                $uploadedFile = $uploadedFiles['cover'];
                // echo "<pre>";print_r($uploadedFile);echo "</pre>";
                if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                    $filename = $this->moveUploadedFile( $uploadedFile);
                    $entrevista->archivo = $filename;
                }
            }

            $entrevista->save();
            echo json_encode($entrevista->toArray());


        }
        //return $this->container->view->render($response, 'back/entrevistas-back-'.$arg['type'].'.twig',$data);
    }

    public function toggleVisibility($request, $response){
        $datos = $request->getParsedBody();
        //print_r();
        if(!empty($datos)){
            $entrevista = Entrevista::find($datos[id]);

            $entrevista->visible = ($datos['visible']!='false')?1:0;
            $entrevista->save();
            echo json_encode($entrevista->toArray());


        }
    }

    private function moveUploadedFile( UploadedFile $uploadedFile)
    {

        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $folders = array(
            'audio'=>array('mp3','aac','ogg','wav','vorbis'),
            'image'=>array('jpg','jpeg','bmp','png','gif','webp'),
            'video'=>array('mp4','3gp','wmv','mov','MPEG-4','flv','avi','mkv','webm')
        );

        $destFolder = "";

        foreach ($folders as $k=>$v){
            if(in_array($extension,$v)){
                $destFolder = $k;
                break;
            }
        }
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);
        $fileFullName  = "img/uploads/covers". DIRECTORY_SEPARATOR . $filename;
        echo $fileFullName;
        $uploadedFile->moveTo($fileFullName);

        return $fileFullName;
    }
}