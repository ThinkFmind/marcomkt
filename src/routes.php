<?php

use App\Controllers\EntradasController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
return function(App $app){
    $app->get('/', function ($request, $response, $args) {
        return $this->view->render($response, 'home.twig');
    })->setName('home');

    $app->get('/entrevistas','WebController:indexEntrevistas')->setName('entrevistas');

    $app->get('/no-contamos','WebController:indexNoContamos')->setName('contamos');

    $app->get('/contacto', function ($request, $response, $args) {
        return $this->view->render($response, 'contacto.twig');
    })->setName('contacto');

    $app->get('/terminos-condiciones', function ($request, $response, $args) {
        return $this->view->render($response, 'terminos-condiciones.twig');
    })->setName('termcond');

    $app->get('/nota/{id}','WebController:showNote' )->setName('notas');

    $app->post('/create-entry','WebController:createEntrada')->setName('create-entry');
    $app->get('/bdoor/login', 'UserController:login')->setName('login');
    $app->get('/mail', 'UserController:sendMail')->setName('mail');
    $app->post('/bdoor/login-in', 'UserController:enterLogin')->setName('login-in');
    $app->group('/bdoor/',function (App $app){

        $app->get('home', 'EntradasController:index')->setName('back-home');
        $app->get('entrevistas-back/[{type}]', 'EntrevistasController:index')->setName('back-video');
        $app->post('create-video', 'EntrevistasController:createEntrevista')->setName('create-video');
        $app->post('toggle-vis', 'EntrevistasController:toggleVisibility')->setName('toggle-vis');
        $app->post('toggle-aprob', 'EntradasController:toggleVisibility')->setName('toggle-aprob');
        $app->post('create-audio', 'EntrevistasController:createEntrevista')->setName('create-video');
        $app->get('edit-entry/{id}', 'EntradasController:editEntrada')->setName('entrada-edit');
        $app->post('save-entry', 'EntradasController:saveEntradaEdit')->setName('save-edit');
    })->add(function ($request, $response, $next) {

        session_start();
        //echo $_SESSION['user'];
        /*if(empty($_SESSION['user'])){
            $uri = $request->getUri()->withPath($this->router->pathFor('login'));
            $response = $response->withRedirect($uri);
        }else{*/
            $request = $request->withAttribute('user', $_SESSION['user']);
            $response = $next($request, $response);
        //}

        return $response;
    });
};
